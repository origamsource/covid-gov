﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:AS="http://schema.advantages.cz/AsapFunctions"
	xmlns:date="http://exslt.org/dates-and-times" 
	xmlns:str="http://exslt.org/strings" 
	exclude-result-prefixes="AS date str">

	<xsl:variable name="topicId" select="/ROOT/Topic/@Id"/>
	
	<xsl:template match="ROOT">
		<ROOT>
			<xsl:apply-templates select="FAQImport"/>
		</ROOT>
	</xsl:template>

	<xsl:template match="FAQImport">
		<xsl:variable name="text" select="Text"/>
		<xsl:variable name="firstCharacterIndex">
			<xsl:choose>
				<xsl:when test="substring($text, 1, 1) = '&quot;'">2</xsl:when>
				<xsl:otherwise>1</xsl:otherwise>
			</xsl:choose> 
		</xsl:variable>
		<xsl:variable name="lastCharacterIndex">
			<xsl:choose>
				<xsl:when test="substring($text, string-length($text), 1) = '&quot;'">
					<xsl:value-of select="string-length($text) - $firstCharacterIndex"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="string-length($text)"/>
				</xsl:otherwise>
			</xsl:choose> 
		</xsl:variable>
		<xsl:for-each select="str:split(substring($text, $firstCharacterIndex, $lastCharacterIndex), '&#10;')">
			<xsl:if test="normalize-space(.)">
				<TopicFAQ 
					Id="{AS:GenerateId()}"
					refTopicId="{$topicId}"
					Question="{concat(normalize-space(substring-before(., '?')), '?')}"
					Answer="{normalize-space(substring-after(., '?'))}"
				/>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>