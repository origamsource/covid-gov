﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:AS="http://schema.advantages.cz/AsapFunctions"
	xmlns:date="http://exslt.org/dates-and-times" 
	xmlns:str="http://exslt.org/strings" 
	exclude-result-prefixes="AS date str">
	
	<xsl:template match="ROOT">
		<xsl:apply-templates select="Topic"/>
	</xsl:template>

	<xsl:template match="Topic">
		<xsl:variable name="category" 
			select="AS:LookupValue('188e7334-a833-4de1-8fde-816b77c4f9c7', @refCategoryId)"/>
		<xsl:variable name="title" 
			select="@Name"/>
		<html lang="cs">
			<head>
			    <title><xsl:value-of select="$title"/></title>
			    <meta charset="utf-8"/>
			    <meta name="viewport" content="width=375, initial-scale=1, shrink-to-fit=no"/>
			
			    <!--<link rel="stylesheet" media="screen" href="https://designsystem.gov.cz/assets/mv/css/front.css">-->
			    <link rel="stylesheet" media="screen" href="/customAssets/preview/front.css"/>
			    <link rel="stylesheet" media="screen" href="/customAssets/preview/styles.css"/>
			    <link href="https://fonts.googleapis.com/css2?family=Roboto%3Awght%40100%3B300%3B400%3B500%3B700%3B900&amp;display=swap" rel="stylesheet" media="none" onload="if(media!='all')media='all'"/>
			</head>
		<body class="page page--detail">
		    <div class="header">
		        <div class="container">
		            <div class="row">
		                <div class="col-6 d-flex align-items-center">
		                    <a href="/">
		                        <img src="/customAssets/preview/logo.png" alt="logo"/>
		                    </a>
		                </div>
		                <div class="col-6 d-flex justify-content-end align-items-center">
		                    <a href="#">
		                        <svg width="73" height="32" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M48 12h18M54 20h12" stroke="#fff" stroke-width="2" stroke-linecap="round"/><path d="M2.79 11.469l2.46 6.539 2.455-6.54h1.916V20H8.145v-2.813l.146-3.761L5.771 20h-1.06l-2.514-6.568.147 3.755V20H.867v-8.531H2.79zm14.075 4.722h-3.504v2.625h4.096V20h-5.578v-8.531h5.537v1.195h-4.055v2.356h3.504v1.171zM25.932 20h-1.483l-3.803-6.053V20h-1.482v-8.531h1.482l3.815 6.076v-6.076h1.47V20zm8.492-8.531v5.7c0 .907-.291 1.626-.873 2.157-.578.527-1.35.791-2.315.791-.976 0-1.752-.26-2.326-.78-.574-.523-.861-1.247-.861-2.173V11.47h1.476v5.707c0 .57.145 1.006.434 1.306.289.301.715.452 1.277.452 1.141 0 1.711-.602 1.711-1.805v-5.66h1.477z" fill="#fff"/></svg>
		                    </a>
		                </div>
		            </div>
		            <form class="header__search">
		                <input type="search" placeholder="Hledejte opatření nebo situaci"></input>
		                <button type="submit" class="d-flex align-items-center justify-content-center">
		                    <svg width="16" height="15" fill="none" xmlns="http://www.w3.org/2000/svg"><circle cx="6" cy="6" r="5" stroke="#3B3B3B" stroke-width="2"/><path d="M10.5 9.5l3.09 3.111" stroke="#3B3B3B" stroke-width="2" stroke-linecap="square"/></svg>
		                </button>
		            </form>
		        </div>
		    </div>
		
		    <main class="pb-2">
		        <div class="hero">
		            <div class="container">
		                <nav class="breadcrumbs" aria-label="breadcrumb">
		                    <ol class="breadcrumb">
		                        <li class="breadcrumb__item"><a href="#" class="breadcrumb__link">Úvod</a></li>
		                        <li class="breadcrumb__item"><a href="#" class="breadcrumb__link"><xsl:value-of select="$category"/></a></li>
		                        <li class="breadcrumb__item breadcrumb__item--active" aria-current="page"><span class="breadcrumb__link"><xsl:value-of select="$title"/></span></li>
		                    </ol>
		                </nav>
		                <h2 class="color-white">
		                    <xsl:value-of select="$title"/>
		                </h2>
		            </div>
		        </div>
		        
		        <div class="container">
		            <div class="box mb-2">
		                <xsl:apply-templates select="AS:ToXml(concat('&lt;div&gt;', str:replace(str:replace(@Description, '&amp;nbsp;', '&amp;#160;'), '&lt;br&gt;', '&lt;br /&gt;'), '&lt;/div&gt;'))"/>
		                <xsl:apply-templates select="TopicRelatedLink"/>
		            </div>
		
		        	<xsl:if test="count(TopicFAQ) > 0">
			            <div class="box mb-2 box--blue-light">
			                <h2>
			                    <strong>Časté dotazy</strong> k tématu
			                </h2>
			                <xsl:apply-templates select="TopicFAQ"/>
			            </div>
		        	</xsl:if>
		
		        	<xsl:if test="count(TopicRelatedTopic) > 0">
			            <div class="box box--blue-dark">
			                <h2>
			                    <strong>Podobná</strong> témata
			                </h2>
			
			                <div class="box__section pb-0 mb-1">
				                <xsl:apply-templates select="TopicRelatedTopic"/>
			                </div>
			            </div>
		        	</xsl:if>
		        </div>
		    </main>
		    <footer>
		        <img src="/customAssets/preview/footer.svg"/>
		    </footer>
		    <script src="/customAssets/preview/scripts.js"></script>
		</body>
		</html>
	</xsl:template>

	<xsl:template match="TopicRelatedLink">
		<a href="{@Url}"><xsl:value-of select="@Name"/></a>
	</xsl:template>

	<xsl:template match="TopicFAQ">
        <div class="box__section pb-0 mb-1 js-collapse-wrapper">
            <h3>
                <a href="#" class="d-flex align-items-center justify-content-between js-collapse-trigger">
                    <xsl:value-of select="@Question"/>
                   <svg width="24" height="24" fill="none" xmlns="http://www.w3.org/2000/svg"><rect x=".5" y=".5" width="23" height="23" rx="2.5" fill="#ECAE1A" stroke="#ECAE1A"/><path stroke="#fff" stroke-width="2" d="M12 7v10M7 12h10"/></svg>
                </a>
            </h3>
            <p class="is-hidden js-collapse-content">
                    <xsl:value-of select="@Answer"/>
            </p>
        </div>
	</xsl:template>
	
	<xsl:template match="TopicRelatedTopic">
	    <a href="#" class="mb-2">
	        <h3 class="d-flex align-items-center justify-content-between">
	            <span class="d-flex align-items-center">
	                <span><xsl:value-of select="@Name"/></span>
	            </span>
	            <svg width="6" height="9" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M2.828 4.243L0 7.07l1.414 1.414 2.829-2.828 1.414-1.414-1.414-1.415L1.414 0 0 1.414l2.828 2.829z" fill="#ECAE1A"/></svg>
	        </h3>
	    </a>
	</xsl:template>

	<xsl:template match="*">
		<xsl:copy>
			<xsl:copy-of select="@*"/>
			<xsl:if test="preceding-sibling::node()[1][self::text() = ' ']">
				<xsl:value-of select="' '"/>
			</xsl:if>
			<xsl:apply-templates />
		</xsl:copy>
	</xsl:template>
	
	<xsl:template match="text()">
		<xsl:value-of select="."/>
	</xsl:template>
</xsl:stylesheet>