﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:AS="http://schema.advantages.cz/AsapFunctions"
	xmlns:date="http://exslt.org/dates-and-times"
	xmlns:str="http://exslt.org/strings"
	exclude-result-prefixes="AS date str">

	<xsl:key name="topics-by-category" match="product" use="@category"/>

	<xsl:template match="ROOT">
		<ROOT>
			<xsl:variable name="sheet" select="AS:HttpRequest(AS:GetConstant('GoogleSheetUrl'))"/>
			<xsl:variable name="table" select="concat('&lt;table', substring-before(substring-after($sheet, '&lt;table'), '&lt;/table&gt;'), '&lt;/table&gt;')"/>
			<xsl:variable name="tableXml" select="AS:ToXml(str:replace($table, '&lt;br&gt;', '&lt;br /&gt;'))"/>
			<xsl:for-each select="$tableXml/table/tbody/tr[position() > 1]">
				<xsl:if test="string(td[5])">
					<xsl:variable name="categoryId" select="AS:LookupValue('d614b27d-078a-437a-b138-08e86c360dfd', td[1])"/>
					<xsl:if test="not($categoryId)">
						<xsl:message terminate="yes">Kategorie nenalezena: <xsl:value-of select="td[1]"/></xsl:message>
					</xsl:if>
					<Topic 
						Id="{AS:GenerateId()}"
						refCategoryId="{$categoryId}"
						Name="{normalize-space(td[2])}"
					>
						<xsl:attribute name="Description">
							<xsl:variable name="result">
								<xsl:apply-templates select="td[5]"/>
							</xsl:variable> 
							<xsl:value-of select="AS:ProcessMarkdown($result)"/>
						</xsl:attribute>
						<xsl:call-template name="FAQImport">
							<xsl:with-param name="text" select="AS:NodeToString(td[6])"/>
						</xsl:call-template>
					</Topic>
				</xsl:if>
			</xsl:for-each>
		</ROOT>
	</xsl:template>


	<xsl:template match="td">
		<xsl:apply-templates/>
	</xsl:template>

	<xsl:template match="p">
		<xsl:variable name="result">
			<xsl:apply-templates/>
		</xsl:variable>
		<xsl:value-of select="concat(normalize-space($result), '&#10;&#13;')"/>
	</xsl:template>

	<xsl:template match="br">
		<xsl:value-of select="'&#10;&#13;'"/>
	</xsl:template>

	<xsl:template match="strong">
		<xsl:if test="name(preceding-sibling::*[1]) != 'p'">
			<xsl:value-of select="'**'"/>
		</xsl:if>
			<xsl:apply-templates/>
		<xsl:if test="name(preceding-sibling::*[1]) != 'p'">
			<xsl:value-of select="'**'"/>
		</xsl:if>
	</xsl:template>

	<xsl:template match="em">
		<xsl:value-of select="'*'"/>
			<xsl:apply-templates/>
		<xsl:value-of select="'*'"/>
	</xsl:template>

	<xsl:template match="span">
		<xsl:choose>
			<xsl:when test=". = 'Pravidla a doporučení'">
				<xsl:value-of select="concat('##', ., '&#10;&#13;')"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:variable name="bold">
					<xsl:if test="contains(@style, 'font-weight:bold')">**</xsl:if>
				</xsl:variable>
				<xsl:variable name="result">
					<xsl:apply-templates/>
				</xsl:variable>
				<xsl:value-of select="concat($bold, $result, $bold)"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="a">
		<xsl:value-of select="concat('[', .,'](', @href,')')"/>
	</xsl:template>

	<xsl:template match="text()">
      <xsl:value-of select="."/>
   </xsl:template>

	<xsl:template name="FAQImport">
		<xsl:param name="text"/>
		<xsl:variable name="brRemoved" select="str:replace($text, '&lt;br /&gt;', '&#10;')"/>
		<xsl:variable name="nodesRemoved" select="AS:ToXml($brRemoved)"/>
		
		<xsl:for-each select="str:split($nodesRemoved, '&#10;')">
			<xsl:if test="normalize-space(.)">
				<TopicFAQ 
					Id="{AS:GenerateId()}"
					Question="{concat(normalize-space(substring-before(., '?')), '?')}"
					Answer="{normalize-space(substring-after(., '?'))}"
				/>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>