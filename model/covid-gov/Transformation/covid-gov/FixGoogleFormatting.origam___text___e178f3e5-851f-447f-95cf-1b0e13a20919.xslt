﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:AS="http://schema.advantages.cz/AsapFunctions"
	xmlns:date="http://exslt.org/dates-and-times"
	xmlns:str="http://exslt.org/strings"
	exclude-result-prefixes="AS date str">

	<xsl:template match="ROOT">
		<ROOT>
			<xsl:apply-templates select="Topic"/>
		</ROOT>
	</xsl:template>

	<xsl:template match="Topic">
		<xsl:copy>
			<xsl:copy-of select="@*"/>
			<xsl:variable name="descriptionAddRootElement" select="concat('&lt;div&gt;', @Description, '&lt;/div&gt;')"/>
			<xsl:variable name="descriptionFixedBR" select="str:replace($descriptionAddRootElement, '&lt;br&gt;', '&lt;/p&gt;&lt;p&gt;')"/>
			<xsl:variable name="descriptionFixedNbsp" select="str:replace($descriptionFixedBR, '&amp;nbsp;', '&amp;#160;')"/>
			<xsl:variable name="descriptionNodeset" select="AS:ToXml($descriptionFixedNbsp)"/>
			<xsl:variable name="markdown">
				<xsl:apply-templates select="$descriptionNodeset"/>
			</xsl:variable>
			<xsl:attribute name="Description">
				<xsl:value-of select="AS:ProcessMarkdown($markdown)"/>
			</xsl:attribute>

			<xsl:copy-of select="*"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="div">
		<xsl:apply-templates/>
	</xsl:template>

	<xsl:template match="p">
		<xsl:variable name="result">
			<xsl:apply-templates/>
		</xsl:variable>
		<xsl:value-of select="concat(normalize-space($result), '&#10;&#13;')"/>
	</xsl:template>

	<xsl:template match="strong">
		<xsl:if test="name(preceding-sibling::*[1]) != 'p'">
			<xsl:value-of select="'**'"/>
		</xsl:if>
			<xsl:apply-templates/>
		<xsl:if test="name(preceding-sibling::*[1]) != 'p'">
			<xsl:value-of select="'**'"/>
		</xsl:if>
	</xsl:template>

	<xsl:template match="em">
		<xsl:value-of select="'*'"/>
			<xsl:apply-templates/>
		<xsl:value-of select="'*'"/>
	</xsl:template>

	<xsl:template match="span">
		<xsl:value-of select="' '"/>
			<xsl:apply-templates/>
		<xsl:value-of select="' '"/>
	</xsl:template>

	<xsl:template match="a">
		<xsl:value-of select="concat('[', .,'](', @href,')')"/>
	</xsl:template>

	<xsl:template match="text()">
      <xsl:value-of select="."/>
   </xsl:template>
</xsl:stylesheet>