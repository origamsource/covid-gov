﻿INSERT INTO OrigamApplicationRole (Id, Name, Description, IsSystemRole , RecordCreated)
VALUES ('df332734-c601-4eaa-83cc-254ecaf506f2', 'Measure_New', '', 1, getdate())
-- add to the built-in SuperUser role
INSERT INTO OrigamRoleOrigamApplicationRole (Id, refOrigamRoleId, refOrigamApplicationRoleId, RecordCreated, IsFormReadOnly)
VALUES (newid(), 'e0ad1a0b-3e05-4b97-be38-12ff63e7f2f2', 'df332734-c601-4eaa-83cc-254ecaf506f2', getdate(), 0)