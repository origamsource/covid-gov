﻿CREATE TABLE [Topic] (
	[refCategoryId] UniqueIdentifier NOT NULL,
	[Name] NVarChar(200) NOT NULL,
	[RecordCreatedBy] UniqueIdentifier NULL,
	[RecordUpdatedBy] UniqueIdentifier NULL,
	[Id] UniqueIdentifier NOT NULL PRIMARY KEY ,
	[RecordCreated] DateTime NULL,
	[RecordUpdated] DateTime NULL);
CREATE UNIQUE  INDEX  [ix_Category] ON [Topic] ([refCategoryId] ASC, [Name] ASC);